# Traj_gen_ros

Uses traj_gen to build a trajectory forming a square. The trajectory stops at each corner of the square. 


# Build

To build in a ros workspace along side with traj_gen\

`catkin build`

`source devel/setup.bash`

# Launch

It launches
- rviz to visualize the square trajectory
- rqt_reconfigure to change online the maximum velocity from 0.001 to 1.7 (If 0 then ruckig cannot find a solution)

`roslaunch traj_gen_ros run.launch`
