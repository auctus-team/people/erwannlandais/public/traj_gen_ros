#!/usr/bin/env python3

from __future__ import print_function
from urllib import request
import rospy

# Brings in the SimpleActionClient
import actionlib
from traj_gen_ros.msg import *
from geometry_msgs.msg import Pose
from traj_gen_ros.srv import *
from std_msgs.msg import *
from tf2_ros import Buffer, TransformListener, TransformBroadcaster
from rospy import Time, Duration


def set_max_velocity(translation=0.0,rotation=0.0):
    setTrajectoryLimits = rospy.ServiceProxy('/panda/setTrajectoryLimits', TrajectoryLimits)
    setTrajectoryLimits(TrajectoryLimitsRequest(velocity_limits=[translation,rotation],acceleration_limits=[0.0,0.0],jerk_limits=[0.0,0.0]))

def set_max_acceleration(translation=0.0,rotation=0.0):
    setTrajectoryLimits = rospy.ServiceProxy('/panda/setTrajectoryLimits', TrajectoryLimits)
    setTrajectoryLimits(TrajectoryLimitsRequest(acceleration_limits=[translation,rotation],velocity_limits=[0.0,0.0],jerk_limits=[0.0,0.0]))

def set_max_jerk(translation=0.0,rotation=0.0):
    setTrajectoryLimits = rospy.ServiceProxy('/panda/setTrajectoryLimits', TrajectoryLimits)
    setTrajectoryLimits(TrajectoryLimitsRequest(jerk_limits=[translation,rotation],velocity_limits=[0.0,0.0],acceleration_limits=[0.0,0.0]))

def get_link_pose(link_name):
    tfBuffer = Buffer()
    tfListener = TransformListener(tfBuffer)

    for i in range(0,20):
            tf_pose_to_go =tfBuffer.lookup_transform("world",
                                        link_name, Time(0),
                                        Duration(20))  
    robot_pose = Pose()
    robot_pose.position.x = tf_pose_to_go.transform.translation.x  
    robot_pose.position.y =  tf_pose_to_go.transform.translation.y
    robot_pose.position.z = tf_pose_to_go.transform.translation.z 
    robot_pose.orientation.x = tf_pose_to_go.transform.rotation.x 
    robot_pose.orientation.y = tf_pose_to_go.transform.rotation.y 
    robot_pose.orientation.z = tf_pose_to_go.transform.rotation.z 
    robot_pose.orientation.w = tf_pose_to_go.transform.rotation.w 
    return robot_pose

def go_to_pose(pose):
    # Creates the SimpleActionClient, passing the type of the action
    # (FibonacciAction) to the constructor.
    client = actionlib.SimpleActionClient('/trajectory_action', traj_gen_ros.msg.TrajectoryAction)

    # Waits until the action server has started up and started
    # listening for goals.
    rospy.logerr("waiting for server")
    client.wait_for_server()
    rospy.logerr("client server")
    # Creates a goal to send to the action server.
    goal = traj_gen_ros.msg.TrajectoryGoal(pose=pose)
    
    goal.pose = pose
    # Sends the goal to the action server.
    client.send_goal(goal)

    # Waits for the server to finish performing the action.
    client.wait_for_result()

    # Prints out the result of executing the action
    return client.get_result()  # A FibonacciResult


