#pragma once

#include <ros/ros.h>
#include <actionlib/server/simple_action_server.h>
#include <traj_gen_ros/TrajectoryAction.h>
#include <traj_gen_ros/traj_gen_ros.hpp>

class TrajectoryAction
{
protected:

  ros::NodeHandle nh_;
  actionlib::SimpleActionServer<traj_gen_ros::TrajectoryAction> as_; // NodeHandle instance must be created before this line. Otherwise strange error occurs.
  std::string action_name_;
  // create messages that are used to published feedback/result
  traj_gen_ros::TrajectoryFeedback feedback_;
  traj_gen_ros::TrajectoryResult result_;
  std::shared_ptr<traj_gen_ros::TrajGenRos> trajectory;

public:

  TrajectoryAction(std::string name, std::shared_ptr<traj_gen_ros::TrajGenRos> trajectory ) :
    as_(nh_, name, boost::bind(&TrajectoryAction::executeCB, this, _1), false),
    action_name_(name)
  {
    this->trajectory = trajectory;
    as_.start();
  }

  ~TrajectoryAction(void)
  {
  }

  void executeCB(const traj_gen_ros::TrajectoryGoalConstPtr &goal)
  {
    ros::Rate r(1);
    bool success = true;
    Eigen::Affine3d goal_pose;
    Affine aff;
    tf::poseMsgToEigen(goal->pose, goal_pose);
    aff.setFrame(goal_pose);
    Waypoint wp(aff);
    WaypointMotion wpm;
    wpm.waypoints.push_back(wp);
    success = trajectory->buildTrajectory(wpm);

    while (trajectory->getTrajectoryResult() == Result::Working)
    {
      // check that preempt (meaning cancelled) has not been requested by the client
      if (as_.isPreemptRequested() || !ros::ok())
      {
          ROS_INFO("%s: Preempted", action_name_.c_str());
          // set the action state to preempted
          as_.setPreempted();
          success = false;
      }
    //   feedback_.time_ratio = trajectory->getTimeProgress() ;
    //   // publish the feedback
    //   as_.publishFeedback(feedback_);
    }
   

    if(trajectory->getTrajectoryResult() != Result::Finished)
    {
      as_.setSucceeded(result_);
    }
    else{
        as_.setAborted(result_);
    }
  }
};
