#pragma once

#include "traj_gen_ros/traj_gen_ros.hpp"
#include "traj_gen_ros/traj_gen_ros_action_server.hpp"

class TrajectoryInterface
{
  public:
  TrajectoryInterface(){
    interface = std::shared_ptr<traj_gen_ros::TrajGenRos>(new traj_gen_ros::TrajGenRos);
    boost::thread t1(&TrajectoryInterface::action_thread,this,interface);  
  }

  void action_thread(std::shared_ptr<traj_gen_ros::TrajGenRos> interface)
{
    while(!interface->isInitialized())
    {
        ROS_DEBUG_STREAM_THROTTLE(1,"Wait for trajectory manager to be initialized");
    }
    TrajectoryAction trajectory_action("/trajectory_action", interface);
    while(ros::ok())
    {
        //spin 
    }
}
  std::shared_ptr<traj_gen_ros::TrajGenRos> interface;

};