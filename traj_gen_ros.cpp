#include <ruckig_cart/ruckig_cart.hpp>
#include "ros/ros.h"
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/TwistStamped.h"
#include "eigen_conversions/eigen_msg.h"
#include <dynamic_reconfigure/server.h>
#include <ruckig_cart/ConfigConfig.h>

TrajectoryGenerator trajectory;
CartesianState current_state;

void callback(traj_dyn_reconfigure::ConfigConfig &config, uint32_t level) {
    trajectory.setMaxTranslationVelocity(config.double_param );
}

int main(int argc, char **argv){

    ros::init(argc, argv, "ruckig_cart");
    ros::NodeHandle n;

    ros::Publisher pose_pub = n.advertise<geometry_msgs::PoseStamped>("Pose", 1000);
    ros::Publisher velocity_pub = n.advertise<geometry_msgs::TwistStamped>("Velocity", 1000);
    ros::Publisher acceleration_pub = n.advertise<geometry_msgs::TwistStamped>("Acceleration", 1000);
    ros::Publisher jerk_pub = n.advertise<geometry_msgs::TwistStamped>("Jerk", 1000);

    geometry_msgs::PoseStamped pose_msg;
    geometry_msgs::TwistStamped velocity_msg;
    geometry_msgs::TwistStamped acceleration_msg;
    geometry_msgs::TwistStamped jerk_msg;

    dynamic_reconfigure::Server<traj_dyn_reconfigure::ConfigConfig> server;
    dynamic_reconfigure::Server<traj_dyn_reconfigure::ConfigConfig>::CallbackType f;

    f = boost::bind(&callback, _1, _2);
    server.setCallback(f);

    pose_msg.header.frame_id =
    velocity_msg.header.frame_id =
    acceleration_msg.header.frame_id =
    jerk_msg.header.frame_id = "world";

    double control_cycle = 0.001;
    ros::Rate loop_rate(1.0/control_cycle);
    
    
    auto wp1 = RepeatMotion ({
         LinearMotion(1,0,0),
         LinearMotion(1,1,0),
         LinearMotion(0,1,0),
         LinearMotion(0,0,0)
    },-1);

    // auto wp1 = RepeatMotion ({
    //     LinearMotionRelative(1,0,0),
    //     HoldPosition(5),
    //     LinearMotionRelative(-1,0,0)
    // },-1);


    current_state.pose.translation() = Eigen::Vector3d(0.0,0,0);

    if (!trajectory.build(current_state,wp1))
    {
        std::cout << "Trajectory input error" << std::endl;
        trajectory.printInput();
    }

    int i = 0;
    while(ros::ok())
    {
        if (i>200)
        {
            Result result = trajectory.update();
            if(result == Result::Working)
            {
                current_state = trajectory.getNextState();
                tf::poseEigenToMsg(current_state.pose,pose_msg.pose);
                tf::twistEigenToMsg(current_state.velocity,velocity_msg.twist);
                tf::twistEigenToMsg(current_state.acceleration,acceleration_msg.twist);
                tf::twistEigenToMsg(current_state.jerk,jerk_msg.twist);
            }
            else
            {
                std::cout << " Result not working"; 
                ros::shutdown();
            }
        }
        // if (i == 5000)
        //     trajectory.setMaxTranslationVelocity(0.2);
        ++i;

        pose_msg.header.stamp =
        velocity_msg.header.stamp =
        acceleration_msg.header.stamp =
        jerk_msg.header.stamp = ros::Time::now();

        pose_pub.publish(pose_msg);
        velocity_pub.publish(velocity_msg);
        acceleration_pub.publish(acceleration_msg);
        jerk_pub.publish(jerk_msg);

        ros::spinOnce();
        loop_rate.sleep();
    }
}